#!/usr/bin/env python3

import os
import wx
from sys import platform

class FrameMain(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, parent = None, title = "GiNkS's app", size = (720, 240),
            style = wx.DEFAULT_FRAME_STYLE & ~(wx.RESIZE_BORDER |  wx.MAXIMIZE_BOX))

        # initialize background panel and its sizers
        hSzrBG = wx.BoxSizer(wx.HORIZONTAL)
        vSzrBG = wx.BoxSizer(wx.VERTICAL)
        pnlBG = wx.Panel(self)
        #pnlBG.SetBackgroundColour(wx.Colour(wx.WHITE))

        # initialize sizers
        vSzr = wx.BoxSizer(wx.VERTICAL)
        vSzrBtm = wx.BoxSizer(wx.VERTICAL)
        hSzrBtm = wx.BoxSizer(wx.HORIZONTAL)
        vSzrBtmR = wx.BoxSizer(wx.VERTICAL)
        hSzrTop = wx.FlexGridSizer(5)

        # initialize windows for top sizer
        txtFileInput = wx.StaticText(pnlBG, label = "Choose input file:")
        self.fldFileInput = wx.TextCtrl(pnlBG, size = (520, -1), style = wx.TE_PROCESS_ENTER)
        self.btnFileInputDialog = wx.Button(pnlBG, label = "...", size = (40, -1))
        txtFileOutput = wx.StaticText(pnlBG, label = "Choose output file:")
        self.fldFileOutput = wx.TextCtrl(pnlBG, size = (520, -1), style = wx.TE_PROCESS_ENTER)
        self.btnFileOutputDialog = wx.Button(pnlBG, label = "...", size = (40, -1))

        # fill top sizer
        hSzrTop.Add(txtFileInput, 0, wx.ALIGN_CENTER_VERTICAL | wx.BOTTOM, 5)
        hSzrTop.AddSpacer(8)
        hSzrTop.Add(self.fldFileInput, 0, wx.ALIGN_CENTER_VERTICAL | wx.BOTTOM, 5)
        hSzrTop.AddSpacer(8)
        hSzrTop.Add(self.btnFileInputDialog, 0, wx.ALIGN_CENTER_VERTICAL | wx.BOTTOM, 5)
        hSzrTop.Add(txtFileOutput, 0, wx.ALIGN_CENTER_VERTICAL)
        hSzrTop.AddSpacer(-1)
        hSzrTop.Add(self.fldFileOutput, 0, wx.ALIGN_CENTER_VERTICAL)
        hSzrTop.AddSpacer(-1)
        hSzrTop.Add(self.btnFileOutputDialog, 0, wx.ALIGN_CENTER_VERTICAL)

        # initialize windows for bottom sizer
        self.fldLog = wx.TextCtrl(pnlBG, value = "",
            style = wx.TE_READONLY | wx.TE_MULTILINE)

        # initialize windows for bottom right sizer
        self.btnRun = wx.Button(pnlBG, label = "Run treatment")
        self.btnStop = wx.Button(pnlBG, label = "Stop treatment")
        self.btnAbout = wx.Button(pnlBG, label = "About")

        # fill bottom right sizer
        vSzrBtmR.AddStretchSpacer(1)
        vSzrBtmR.Add(self.btnRun, 0, wx.ALIGN_CENTER_HORIZONTAL)
        vSzrBtmR.AddSpacer(5)
        vSzrBtmR.Add(self.btnStop, 0, wx.ALIGN_CENTER_HORIZONTAL)
        vSzrBtmR.AddSpacer(5)
        vSzrBtmR.Add(self.btnAbout, 0, wx.ALIGN_CENTER_HORIZONTAL)
        vSzrBtmR.AddStretchSpacer(1)

        # fill bottom horizontal sizer
        hSzrBtm.Add(self.fldLog, 4, wx.EXPAND)
        hSzrBtm.AddSpacer(10)
        hSzrBtm.Add(vSzrBtmR, 1, wx.EXPAND)

        # fill bottom vertical sizer
        vSzrBtm.Add(hSzrBtm, 1, wx.EXPAND)

        # fill main sizer
        vSzr.AddSpacer(10)
        vSzr.Add(hSzrTop, 0, wx.ALIGN_CENTER_HORIZONTAL)
        vSzr.AddSpacer(10)
        vSzr.Add(vSzrBtm, 1, wx.EXPAND | wx.RIGHT | wx.LEFT, 10)
        vSzr.AddSpacer(10)

        # activate main sizer on background panel
        pnlBG.SetSizer(vSzr)

        # fill background sizer
        vSzrBG.Add(pnlBG, 1, wx.EXPAND)
        hSzrBG.Add(vSzrBG, 1, wx.EXPAND)

        # activate background sizer on this frame
        self.SetSizer(hSzrBG)

        # bind actions and controls
        self.btnFileInputDialog.Bind(wx.EVT_BUTTON, self.OnFileInputDialog)
        self.fldFileInput.Bind(wx.EVT_CHAR, self.OnFldFileInputEnter)
        self.btnFileOutputDialog.Bind(wx.EVT_BUTTON, self.OnFileOutputDialog)
        self.fldFileOutput.Bind(wx.EVT_CHAR, self.OnFldFileOutputEnter)
        self.btnRun.Bind(wx.EVT_BUTTON, self.OnRun)
        self.btnStop.Bind(wx.EVT_BUTTON, self.OnStop)

        # declare and initialize some triggers, spruce up some windows
        self.trFileInput = False
        self.trFileOutput = False

        # self.fldLog.SetCanFocus(False)
        # self.fldLog.DisableFocusFromKeyboard()

        self.btnStop.Enable(False)
        self.btnAbout.Enable(False)

        # show this frame
        self.Show(True)

        # some log thing
        self.fldLog.AppendText("Welcome to GiNkS's app!\n")

    def OnFileInputDialog(self, e):
        dlg = wx.FileDialog(self, "Choose an input file", "", "", "*.*",
                            wx.FD_OPEN)
        if dlg.ShowModal() == wx.ID_OK:
            self.fldFileInput.ChangeValue(dlg.GetPath())
            if self.fldFileOutput.IsModified() == False:
                self.fldFileOutput.ChangeValue(dlg.GetDirectory() +
                                               (r'\output.mp4' if platform == "win32"
                                           else r'/output.mp4'))
                self.fldFileOutput.MarkDirty()
        dlg.Destroy()
        self.fldFileOutput.SetFocus();

    def OnFileOutputDialog(self, e):
        dlg = wx.FileDialog(self, "Choose an output file", "", "", "*.*",
                            wx.FD_SAVE)
        if dlg.ShowModal() == wx.ID_OK:
            self.fldFileOutput.ChangeValue(dlg.GetPath())
            self.fldFileOutput.MarkDirty()
        dlg.Destroy()
        self.btnRun.SetFocus();

    def OnFldFileInputEnter(self, e):
        if e.KeyCode == wx.WXK_RETURN:
            self.fldFileOutput.SetFocus()
        else:
            e.Skip()

    def OnFldFileOutputEnter(self, e):
        if e.KeyCode == wx.WXK_RETURN:
            self.OnRun(e)
        else:
            e.Skip()

    def AssumeRun(self):
        self.fldFileInput.Enable(False)
        self.btnFileInputDialog.Enable(False)
        self.fldFileOutput.Enable(False)
        self.btnFileOutputDialog.Enable(False)
        self.btnRun.Enable(False)
        self.btnStop.Enable(True)

        self.btnStop.SetFocus()

    def AssumeStop(self):
        self.fldFileInput.Enable(True)
        self.btnFileInputDialog.Enable(True)
        self.fldFileOutput.Enable(True)
        self.btnFileOutputDialog.Enable(True)
        self.btnRun.Enable(True)
        self.btnStop.Enable(False)

        self.btnRun.SetFocus()

    def OnRun(self, e):
        if not self.fldFileInput.GetValue():
            wx.MessageBox("Input file is not specified.", "Error", wx.ICON_ERROR)
            self.fldFileInput.SetFocus()
            return
        if not os.path.isfile(self.fldFileInput.GetValue()):
            wx.MessageBox("Specified input file does not exist.", "Error", wx.ICON_ERROR)
            self.fldFileInput.SetFocus()
            return
        try:
            with open(self.fldFileInput.GetValue(), "rb") as file: pass
        except:
            wx.MessageBox("Error while reading input file.", "Error", wx.ICON_ERROR)
            self.fldFileInput.SetFocus()
            return

        if not self.fldFileOutput.GetValue():
            wx.MessageBox("Output file is not specified.", "Error", wx.ICON_ERROR)
            self.fldFileOutput.SetFocus()
            return
        try:
            with open(self.fldFileOutput.GetValue(), "wb") as file: pass
        except:
            wx.MessageBox("Error while writing output file.", "Error", wx.ICON_ERROR)
            self.fldFileOutput.SetFocus()
            return

        self.AssumeRun()
        self.fldLog.AppendText("run treatment\n")

        # for i in range(0, 101, 20):
        #     self.fldLog.AppendText(str(i) + "%\n")

        # self.fldLog.AppendText("ready \n")

    def OnStop(self, e):
        self.AssumeStop()
        self.fldLog.AppendText("treatment was interrupted by user\n")

app = wx.App(False)
frameMain = FrameMain()
app.MainLoop()
